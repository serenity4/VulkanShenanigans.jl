module VulkanShenanigans

using XCB, GLFW, Vulkan, Vulkan.VkCore, ResultTypes
using glslang_jll

using Base.Threads: Atomic

using GLFW: Window


const glslang = glslangValidator(identity)

const DEBUG_MESSAGE_SEVERITY = |(DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT,
                                 DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT,
                                 DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT,
                                 DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
                                )

const DEBUG_MESSAGE_TYPES = |(DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT,
                              DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT,
                              DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
                             )

# store this globally for now for safety
const SWAPCHAIN_IMAGE_FORMAT = FORMAT_B8G8R8A8_SRGB


function __init__() end


struct DeviceCapabilities
    surface_capabilities::SurfaceCapabilitiesKHR
    formats::Vector{SurfaceFormatKHR}
    present_modes::Vector{PresentModeKHR}
end

mutable struct Engine
    # ordering is very important here for proper destruction

    physical_device::PhysicalDevice
    instance::Instance
    window::Window
    surface::SurfaceKHR
    device::Device
    device_capabilities::DeviceCapabilities
    command_pool::CommandPool

    # synchronization; each of these get 1 per frame
    image_available::Vector{Semaphore}
    render_finished::Vector{Semaphore}
    in_flight::Vector{Fence}

    render_pass::RenderPass
    pipelines::Vector{Pipeline}
    swapchain::SwapchainKHR
    image_views::Vector{ImageView}
    framebuffers::Vector{Framebuffer}

    # these are explicitly freed in deinit!
    command_buffers::Vector{CommandBuffer}  # 1 per frame (I think)

    graphics_queue_family_indices::Vector{Int}
    present_queue_family_indices::Vector{Int}

    # configuration options
    config_timeout_draw::Int
    max_frames_in_flight::Int

    frame_idx::Int
    frame_resized::Bool

    function Engine(::typeof(undef);
                    timeout_draw::Integer=typemax(Int),
                    max_frames_in_flight::Integer=2,
                   )
        e = new()
        e.graphics_queue_family_indices = Int[]
        e.present_queue_family_indices = Int[]
        e.command_buffers = []
        e.image_available = []
        e.render_finished = []
        e.in_flight = []
        e.config_timeout_draw = timeout_draw
        e.max_frames_in_flight = max_frames_in_flight
        e.frame_idx = 1
        e.frame_resized = false

        e
    end
end


include("glsl.jl")
include("triangle_tutorial.jl")

#====================================================================================================
       TODO:

The following are currently fucked up:
- Deinit is completely fucked... not sure why the builtins aren't workign but something is out
    of order somewhere here... will be painstaking to go through
- Window resizes mostly don't work even though I have verified the callback is indeed being
    called.  Not impossible this has something to do with the fucked up de-init
- Guess I probably need to fix all this before moving on so... fuck...
====================================================================================================#


end
