ENV["JULIA_DEBUG"] = "VulkanShenanigans"

using VulkanShenanigans; const V = VulkanShenanigans
using XCB, GLFW, Vulkan, Vulkan.VkCore

using VulkanShenanigans: Engine
using VulkanShenanigans: record_render_command!, main


scrap() = quote
    e = Engine(undef)

    V.init!(e, 256, 256)
end
