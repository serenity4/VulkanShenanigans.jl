
function initwindow(e::Engine, Δx::Integer=800, Δy::Integer=600;
                    title::AbstractString="vulkan_tutorial")
    GLFW.WindowHint(GLFW.CLIENT_API, GLFW.NO_API)
    GLFW.WindowHint(GLFW.RESIZABLE, false)

    w = GLFW.CreateWindow(Δx, Δy, title)

    GLFW.SetFramebufferSizeCallback(w, (u, x, y) -> (e.frame_resized = true))

    w
end
initwindow!(e::Engine, a...; kw...) = (e.window = initwindow(e, a...; kw...))

# this will give *all* indices, you are free to take the first ones
function find_queue_families!(e::Engine)
    empty!(e.graphics_queue_family_indices)
    empty!(e.present_queue_family_indices)
    # might find multiple but I think that would be an error
    for (i, prop) ∈ enumerate(get_physical_device_queue_family_properties(e.physical_device))
        if prop.queue_flags & QUEUE_GRAPHICS_BIT > 0
            push!(e.graphics_queue_family_indices, i-1)
            continue
        end

        if unwrap(get_physical_device_surface_support_khr(e.physical_device, i-1, e.surface)) > 0
            push!(e.present_queue_family_indices, i-1)
            continue
        end
    end
end

# it only sees my geforce, so we just pick that
pick_physical_device(vki::Instance) = unwrap(enumerate_physical_devices(vki))[1]
pick_physical_device!(e::Engine) = (e.physical_device = pick_physical_device(e.instance))

function make_queue_infos(e::Engine)
    find_queue_families!(e)
    qfis = unique([first(e.graphics_queue_family_indices), first(e.present_queue_family_indices)])
    map(qfi -> DeviceQueueCreateInfo(qfi, [1.0f0]), qfis)
end

function make_logical_device(e::Engine)
    qs = make_queue_infos(e)
    o = create_device(e.physical_device, qs, [], ["VK_KHR_swapchain"]) |> unwrap
    @debug("logical device created")
    o
end
make_logical_device!(e::Engine) = (e.device = make_logical_device(e))

get_graphics_queue(e::Engine) = get_device_queue(e.device, first(e.graphics_queue_family_indices), 0)
get_present_queue(e::Engine) = get_device_queue(e.device, first(e.present_queue_family_indices), 0)

"""
    initinstance(;validate=true, early_messenger_init=false)

Create a Vulkan instance object with reasonable defaults for this context.

If `validate`, will use the validation layer.

If `early_messenger_init` the messenger will be created together with the instnace.  This is useful
if something bad happens at instance creation, but does result in a wall of extra debug messages
that you don't normally want.
"""
function initinstance(;validate::Bool=true, early_messenger_init::Bool=false)
    info = ApplicationInfo(v"0.0.1", v"0.0.1", v"1.3",
                           application_name="vulkan_tutorial", engine_name="tutorial_engine"
                          )
    ext = ["VK_EXT_debug_utils"; GLFW.GetRequiredInstanceExtensions()]
    lyrs = validate ? ["VK_LAYER_KHRONOS_validation"] : []
    Instance(lyrs, ext;
             application_info=info,
             next=make_messenger_create_info(),
            )
end
initinstance!(e::Engine; kw...) = (e.instance = initinstance(;kw...))

function DeviceCapabilities(ph::PhysicalDevice, srf::Ptr{Nothing})
    cap = get_physical_device_surface_capabilities_khr(ph, srf) |> unwrap
    fmts = get_physical_device_surface_formats_khr(ph; surface=srf) |> unwrap
    pms = get_physical_device_surface_present_modes_khr(ph; surface=srf) |> unwrap
    DeviceCapabilities(cap, fmts, pms)
end
DeviceCapabilities(ph::PhysicalDevice, srf::SurfaceKHR) = DeviceCapabilities(ph, srf.vks)
DeviceCapabilities(e::Engine) = DeviceCapabilities(e.physical_device, e.surface)

device_capabilities!(e::Engine) = (e.device_capabilities = DeviceCapabilities(e))

function default_present_mode(dc::DeviceCapabilities)
    if PRESENT_MODE_IMMEDIATE_KHR ∈ dc.present_modes
        PRESENT_MODE_IMMEDIATE_KHR
    else
        PRESENT_MODE_FIFO_KHR
    end
end
default_present_mode(e::Engine) = default_present_mode(e.device_capabilities)

# this is not general enough, but should be ok for now
getextent(e::Engine) = e.device_capabilities.surface_capabilities.current_extent

function make_swapchain(e::Engine)
    # choose immediate... my hypothesis is that gsync should magically clean it up
    sfcapabilities = e.device_capabilities.surface_capabilities
    qfi = [first(e.graphics_queue_family_indices), first(e.present_queue_family_indices)]
    o = create_swapchain_khr(e.device, e.surface,
                             UInt32(sfcapabilities.min_image_count),
                             SWAPCHAIN_IMAGE_FORMAT,  # image format
                             COLOR_SPACE_SRGB_NONLINEAR_KHR,  # color space
                             getextent(e),  # window size, in this case
                             UInt32(1),  # image array layers... don't know what that means
                             IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                             SHARING_MODE_EXCLUSIVE,  # not too sure what this means
                             [],  # queue family indices
                             sfcapabilities.current_transform,
                             COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
                             default_present_mode(e),
                             true,  # means we don't care about pixels that are not drawn
                            ) |> unwrap
    @debug("swapchain created")
    o
end
make_swapchain!(e) = (e.swapchain = make_swapchain(e))

function remake_swapchain!(e::Engine)
    (x, y) = GLFW.GetFramebufferSize(e.window)
    while x == 0 || y == 0
        GLFW.GetFramebufferSize(e.window)
        GLFW.WaitEvents()
    end

    @try device_wait_idle(e.device)

    make_swapchain!(e)
    make_image_views!(e)
    make_framebuffers!(e)
    e
end

function create_window_surface!(e::Engine)
    e.surface = GLFW.CreateWindowSurface(e.instance, e.window)
end

# called within initinstance
function make_messenger_create_info()
    cb = @cfunction(default_debug_callback, UInt32,
                    (DebugUtilsMessageSeverityFlagEXT, DebugUtilsMessageTypeFlagEXT,
                     Ptr{VkCore.VkDebugUtilsMessengerCallbackDataEXT}, Ptr{Cvoid}),
                   )
    DebugUtilsMessengerCreateInfoEXT(DEBUG_MESSAGE_SEVERITY, DEBUG_MESSAGE_TYPES, cb)
end

function make_window_surface!(e::Engine)
    srf = GLFW.CreateWindowSurface(e.instance, e.window)
    e.surface = SurfaceKHR(srf, x -> destroy_surface_khr(x.instance, x), e.instance)
end

get_swapchain_images(e::Engine) = get_swapchain_images_khr(e.device, e.swapchain) |> unwrap

default_component_mapping() = ComponentMapping(ntuple(x -> COMPONENT_SWIZZLE_IDENTITY, 4)...)

default_image_subresource_range() = ImageSubresourceRange(IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1)

function make_image_views(e::Engine)
    map(get_swapchain_images(e)) do img
        create_image_view(e.device, img, IMAGE_VIEW_TYPE_2D, SWAPCHAIN_IMAGE_FORMAT, default_component_mapping(),
                          default_image_subresource_range(),
                         ) |> unwrap
    end
end
make_image_views!(e::Engine) = (e.image_views = make_image_views(e))

function make_shader_module(e::Engine, code::AbstractVector{UInt32})
    create_shader_module(e.device, 4length(code), code) |> unwrap
end
function make_shader_module(e::Engine, fname::AbstractString; kw...)
    make_shader_module(e, glslcompilefile(fname; kw...))
end

_shadersdir(a...) = joinpath(@__DIR__,"glsl",a...)

function triangle_shader_modules(e::Engine)
    o = [make_shader_module(e, _shadersdir("triangle1.vert")), make_shader_module(e, _shadersdir("triangle1.frag"))]
    @debug("created triangle shader modules")
    o
end

function make_render_pass(e::Engine)
    a = [AttachmentDescription(SWAPCHAIN_IMAGE_FORMAT,
                               SAMPLE_COUNT_1_BIT,
                               ATTACHMENT_LOAD_OP_CLEAR,
                               ATTACHMENT_STORE_OP_STORE,
                               ATTACHMENT_LOAD_OP_DONT_CARE,
                               ATTACHMENT_STORE_OP_DONT_CARE,
                               IMAGE_LAYOUT_UNDEFINED,
                               IMAGE_LAYOUT_PRESENT_SRC_KHR
                              )
        ]
    sp = [SubpassDescription(PIPELINE_BIND_POINT_GRAPHICS,
                             [], # input attachments
                             [AttachmentReference(0, IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)], # color attachments
                             [], # preserve attachments
                            )
         ]
    create_render_pass(e.device, a, sp, []) |> unwrap
end
make_render_pass!(e::Engine) = (e.render_pass = make_render_pass(e))

function _default_color_write_mask()
    |(COLOR_COMPONENT_R_BIT, COLOR_COMPONENT_G_BIT, COLOR_COMPONENT_B_BIT, COLOR_COMPONENT_A_BIT)
end

function viewport_from_extent(e::Engine)
    extent = getextent(e)
    Viewport(0, 0, extent.width, extent.height, 0, 1)
end

function make_graphics_pipeline(e::Engine)
    vp = viewport_from_extent(e)
    sc = Rect2D(Offset2D(0, 0), getextent(e))

    vtx = PipelineVertexInputStateCreateInfo([], [])
    ina = PipelineInputAssemblyStateCreateInfo(PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, false)
    vps = PipelineViewportStateCreateInfo(viewports=[vp], scissors=[sc])
    rst = PipelineRasterizationStateCreateInfo(false,  # rasterizer discard enable
                                               false,
                                               POLYGON_MODE_FILL,
                                               FRONT_FACE_CLOCKWISE,
                                               false,  # depth bias enable
                                               0.0f0,  # depth bias constant factor
                                               0.0f0,  # depth bias clamp
                                               0.0f0,  # depth bias slope factor
                                               1.0f0,  # line width
                                              )
    msm = PipelineMultisampleStateCreateInfo(SAMPLE_COUNT_1_BIT, false, 1.0f0, false, false)

    cba = PipelineColorBlendAttachmentState(false, BLEND_FACTOR_ONE, BLEND_FACTOR_ZERO, BLEND_OP_ADD,
                                            BLEND_FACTOR_ONE, BLEND_FACTOR_ZERO, BLEND_OP_ADD,
                                            _default_color_write_mask()
                                           )
    cb = PipelineColorBlendStateCreateInfo(false, LOGIC_OP_COPY, [cba], ntuple(i -> 0.0f0, 4))

    sts = PipelineDynamicStateCreateInfo([DYNAMIC_STATE_VIEWPORT, DYNAMIC_STATE_SCISSOR])

    pl = create_pipeline_layout(e.device, [], []) |> unwrap

    shader_modules = triangle_shader_modules(e)
    shader_stages = [PipelineShaderStageCreateInfo(SHADER_STAGE_VERTEX_BIT, shader_modules[1], "main"),
                     PipelineShaderStageCreateInfo(SHADER_STAGE_FRAGMENT_BIT, shader_modules[2], "main"),
                    ]
    pipeinfo = GraphicsPipelineCreateInfo(shader_stages,
                                          rst,
                                          pl,
                                          0,  # subpass
                                          -1;  # base pipeline index (none exists)
                                          vertex_input_state=vtx,
                                          input_assembly_state=ina,
                                          viewport_state=vps,
                                          multisample_state=msm,
                                          color_blend_state=cb,
                                          dynamic_state=sts,
                                          render_pass=e.render_pass,
                                         )

    pipes, result = create_graphics_pipelines(e.device, [pipeinfo]) |> unwrap
    @info("created graphics pipeline", result)
    pipes
end
make_graphics_pipeline!(e::Engine) = (e.pipelines = make_graphics_pipeline(e))

function make_framebuffers(e::Engine)
    ext = getextent(e)
    map(e.image_views) do v
        create_framebuffer(e.device, e.render_pass, [v], ext.width, ext.height, 1) |> unwrap
    end
end
make_framebuffers!(e::Engine) = (e.framebuffers = make_framebuffers(e))

function make_commandpool(e::Engine)
    CommandPool(e.device, first(e.graphics_queue_family_indices); flags=VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
end
make_commandpool!(e::Engine) = (e.command_pool = make_commandpool(e))

function make_commandbuffers(e::Engine)
    info = CommandBufferAllocateInfo(e.command_pool, COMMAND_BUFFER_LEVEL_PRIMARY, e.max_frames_in_flight)
    allocate_command_buffers(e.device, info) |> unwrap
end
make_commandbuffers!(e::Engine) = (e.command_buffers = make_commandbuffers(e))

# this will be use by drawframe!
function record_render_command!(e::Engine, buf::CommandBuffer, idx::Integer)
    @try begin_command_buffer(buf, CommandBufferBeginInfo())

    clear_color = Float32.((0, 0, 0, 1)) |> ClearColorValue |> ClearValue  # this should be black
    rpinfo = RenderPassBeginInfo(e.render_pass, e.framebuffers[idx], Rect2D(Offset2D(0, 0), getextent(e)), [clear_color])

    cmd_begin_render_pass(buf, rpinfo, SUBPASS_CONTENTS_INLINE)

    #TODO: how do we infer which is the graphics pipeline?
    cmd_bind_pipeline(buf, PIPELINE_BIND_POINT_GRAPHICS, first(e.pipelines))

    cmd_set_viewport(buf, [viewport_from_extent(e)])

    cmd_set_scissor(buf, [Rect2D(Offset2D(0, 0), getextent(e))])

    cmd_draw(buf, 3, 1, 0, 0)

    cmd_end_render_pass(buf)

    @try end_command_buffer(buf)
end
function record_render_command!(e::Engine, cmd_idx::Integer=1, idx::Integer=1)
    record_render_command!(e, e.command_buffers[cmd_idx], idx)
end

function make_sync_objects!(e::Engine)
    e.image_available = map(i -> Semaphore(e.device), 1:e.max_frames_in_flight)
    e.render_finished = map(i -> Semaphore(e.device), 1:e.max_frames_in_flight)
    e.in_flight = map(i -> Fence(e.device; flags=FENCE_CREATE_SIGNALED_BIT), 1:e.max_frames_in_flight)
end

function init!(e::Engine, Δx::Integer=800, Δy::Integer=600;
               validate::Bool=true,
               window_title::AbstractString="vulkan_tutorial",
              )
    @debug("initializing engine", Δx, Δy, window_title)
    initinstance!(e; validate)
    initwindow!(e, Δx, Δy; title=window_title)
    pick_physical_device!(e)
    make_window_surface!(e)
    make_logical_device!(e)
    device_capabilities!(e)
    make_swapchain!(e)
    make_image_views!(e)
    make_render_pass!(e)
    make_graphics_pipeline!(e)
    make_framebuffers!(e)
    make_commandpool!(e)
    make_commandbuffers!(e)
    make_sync_objects!(e)
    e
end

function drawframe!(e::Engine;
                    timeout_draw::Integer=e.config_timeout_draw,
                   )
    idx = e.frame_idx  # convenience

    @try wait_for_fences(e.device, [e.in_flight[idx]], true, UInt(timeout_draw))

    (img, res) = @try acquire_next_image_khr(e.device, e.swapchain, UInt(timeout_draw); semaphore=e.image_available[idx])

    if res ∈ (ERROR_OUT_OF_DATE_KHR, SUBOPTIMAL_KHR)
        @debug("swapchain must be recreated", res)
        remake_swapchain!(e)
        return e
    end

    @try reset_fences(e.device, [e.in_flight[idx]])

    cbuf = e.command_buffers[idx]
    @try reset_command_buffer(cbuf)
    @try record_render_command!(e, cbuf, img+1)

    info = SubmitInfo([e.image_available[idx]], [PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT], [cbuf],
                      [e.render_finished[idx]]
                     )

    @try queue_submit(get_graphics_queue(e), [info]; fence=e.in_flight[idx])

    pinfo = PresentInfoKHR([e.render_finished[idx]], [e.swapchain], [img])

    res = @try queue_present_khr(get_present_queue(e), pinfo)

    if res ∈ (ERROR_OUT_OF_DATE_KHR, SUBOPTIMAL_KHR) || e.frame_resized
        e.frame_resized = false
        remake_swapchain!(e)
    end

    e.frame_idx = mod1(idx+1, e.max_frames_in_flight)

    e
end

function deinit!(e::Engine)
    free_command_buffers(e.device, e.command_pool, e.command_buffers)
    @debug("command buffers freed")
    nothing
end


function main(e::Engine)
    @debug("main engine loop starting")
    while !GLFW.WindowShouldClose(e.window)
        try
            GLFW.PollEvents()
            drawframe!(e)
        catch err
            @error("encoutered error during engine execution loop")
            GLFW.DestroyWindow(e.window)
            rethrow(err)
        end
    end
    @debug("main engine loop terminated")
    # if we reached here it means window should be destroyed
    GLFW.DestroyWindow(e.window)
    GLFW.Terminate()
    @try device_wait_idle(e.device)
    deinit!(e)
end

