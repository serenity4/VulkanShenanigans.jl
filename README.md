# VulkanShenanigans

This repository contains some of my exploits learning about Vulkan and graphics rendering in Julia.

Very little effort has been put into making sure that it works on systems other than the one it was
developed on (nvidia RTX 2080 super on manjaro linux), so even if you find working code, it's
probably not going to work.

## Troubleshooting Notes
Some layers, notably the validation layer, require a later version of `glibcxx` than Julia links to
by default.  A mismatch can result in confusing errors which may report that other libraries are
missing.  You can force Julia to use a specific version by setting the `LD_PRELOAD` environment
variable, for example
```fish
set -gx LD_PRELOAD /usr/lib/libstdc++.so.6
```
